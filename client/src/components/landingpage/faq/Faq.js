import './Faq.css'

const Faq = () => {
  return (
    <div className="container-fluid">
      <div className="margin-content">
      <div className="faq" id="faq">
                <div className="row">
                    <div className="col-md-6">
                        <h1>Frequently Asked Question</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    </div>
                    <div className="col-md-6">
                        <div className="container my-5">
                            <div className="accordion" id="FAQ-accordion">
                                <div className="accordion-item">
                                    <h2 className="accordion-header" id="heading-one">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-one" aria-expanded="false" aria-controls="collapse-one">
                                            <p>Apa saja syarat yang dibutuhkan?</p>
                                        </button>
                                    </h2>
                                    <div id="collapse-one" className="accordion-collapse collapse" aria-labelledby="heading-one" data-bs-parent="#FAQ-accordion">
                                        <div className="accordion-body">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam doloribus quia blanditiis perferendis, dolore dolor delectus corporis, quasi quo et, ea eaque odit voluptatem earum sit non ipsa! Beatae, mollitia. Accusamus ipsa sequi at minima. Assumenda
                                                deserunt distinctio sunt recusandae repellendus id fugit quis culpa nulla harum officia inventore suscipit dolorum animi officiis temporibus ab eveniet cum, ea vero nihil.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="accordion-item">
                                    <h2 className="accordion-header" id="headingTwo">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-two" aria-expanded="false" aria-controls="collapse-two">
                                            <p>Berapa hari minimal sewa mobil lepas kunci?</p>
                                        </button>
                                    </h2>
                                    <div id="collapse-two" className="accordion-collapse collapse" aria-labelledby="heading-two" data-bs-parent="#FAQ-accordion">
                                        <div className="accordion-body">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam doloribus quia blanditiis perferendis, dolore dolor delectus corporis, quasi quo et, ea eaque odit voluptatem earum sit non ipsa! Beatae, mollitia. Accusamus ipsa sequi at minima. Assumenda
                                                deserunt distinctio sunt recusandae repellendus id fugit quis culpa nulla harum officia inventore suscipit dolorum animi officiis temporibus ab eveniet cum, ea vero nihil.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="accordion-item">
                                    <h2 className="accordion-header" id="heading-three">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-three" aria-expanded="false" aria-controls="collapse-three">
                                            <p>Berapa hari sebelumnya sabaiknya booking sewa mobil?</p>
                                        </button>
                                    </h2>
                                    <div id="collapse-three" className="accordion-collapse collapse" aria-labelledby="heading-three" data-bs-parent="#FAQ-accordion">
                                        <div className="accordion-body">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam doloribus quia blanditiis perferendis, dolore dolor delectus corporis, quasi quo et, ea eaque odit voluptatem earum sit non ipsa! Beatae, mollitia. Accusamus ipsa sequi at minima. Assumenda
                                                deserunt distinctio sunt recusandae repellendus id fugit quis culpa nulla harum officia inventore suscipit dolorum animi officiis temporibus ab eveniet cum, ea vero nihil.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="accordion-item">
                                    <h2 className="accordion-header" id="heading-four">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-four" aria-expanded="false" aria-controls="collapse-four">
                                            <p>Apakah Ada biaya antar-jemput?</p>
                                        </button>
                                    </h2>
                                    <div id="collapse-four" className="accordion-collapse collapse" aria-labelledby="heading-four" data-bs-parent="#FAQ-accordion">
                                        <div className="accordion-body">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam doloribus quia blanditiis perferendis, dolore dolor delectus corporis, quasi quo et, ea eaque odit voluptatem earum sit non ipsa! Beatae, mollitia. Accusamus ipsa sequi at minima. Assumenda
                                                deserunt distinctio sunt recusandae repellendus id fugit quis culpa nulla harum officia inventore suscipit dolorum animi officiis temporibus ab eveniet cum, ea vero nihil.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="accordion-item">
                                    <h2 className="accordion-header" id="heading-five">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-five" aria-expanded="false" aria-controls="collapse-five">
                                            <p>Bagaimana jika terjadi kecelakaan</p>
                                        </button>
                                    </h2>
                                    <div id="collapse-five" className="accordion-collapse collapse" aria-labelledby="heading-five" data-bs-parent="#FAQ-accordion">
                                        <div className="accordion-body">
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam doloribus quia blanditiis perferendis, dolore dolor delectus corporis, quasi quo et, ea eaque odit voluptatem earum sit non ipsa! Beatae, mollitia. Accusamus ipsa sequi at minima. Assumenda
                                                deserunt distinctio sunt recusandae repellendus id fugit quis culpa nulla harum officia inventore suscipit dolorum animi officiis temporibus ab eveniet cum, ea vero nihil.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
  );
};

export default Faq;