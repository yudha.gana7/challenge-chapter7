import './CtaBanner.css'

const CtaBanner = () => {
  return (
    <div className="container-fluid">
      <div className="margin-content">
      <div className="cta-banner mx-auto " id="cta-banner">
                <div className="cta-banner-content">
                    <h1>Sewa Mobil di (Lokasimu) Sekarang</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                    <a href="#" className="btn btn-success">Mulai Sewa Mobil</a>
                </div>
            </div>
      </div>
    </div>
  );
};

export default CtaBanner;