import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import Card1 from "./CardTestimonial1";
import Card2 from "./CardTestimonial2";
import Card3 from "./CardTestimonial3";
import "./Testimonial.css";

class Testimonial extends React.Component {
  render() {
    return (
      <>
        <div
          className="container mt-5 pt-5 d-flex flex-column justify-content-center"
          id="testimonial"
        >
          <h2 className="text-center mb-3 fw-bold pt-4">Testimonial</h2>
          <p className="text-center mb-4">
            Berbagai review positif dari para pelanggan kami
          </p>
        </div>
        <OwlCarousel
          className="owl-theme"
          loop={true}
          margin={32}
          nav={true}
          center={true}
          dots={false}
          navText={[
            "<img src='img/Left-button.png' alt='previous' width='32px' height='32px'>",
            "<img src='img/Right-button.png' alt='next' width='32px' height='32px'>",
          ]}
          responsive={{
            0: {
              items: 1
                },
                360: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1000: {
                    items: 2
                },
                1920: {
                    items: 2
                }
          }}
        >
          <Card1 />
          <Card2 />
          <Card3 />
        </OwlCarousel>
      </>
    );
  }
}

export default Testimonial;
