## Binar Car Rental React JS

Repository ini berisi code mengenai code dari challenge binar car rental frontend yang menggunakan ReactJS.

## How To Install
masuk ke direktori client lalu lakukan code dibawah ini
```sh
npm install
```
setelah itu masuk ke direktori server lalu lakukan code dibawah ini
```sh
npm install
```

## How To Run
masuk ke direktori client lalu lakukan code dibawah ini
```sh
npm start
```
setelah itu masuk ke direktori server lalu lakukan code dibawah ini
```sh
npm run start:dev
```
